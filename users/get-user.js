const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

// Get an user by email
exports.getUser = (event, context, callback) => {

    const email = event.pathParameters.email;

    if (!email) {
        return errorResponse('No se ingresó un correo válido.', context.awsRequestId, callback);
    }

    const params = {
        Key: {
            "email": email
        },
        TableName: process.env.TABLE_NAME
    };

    ddb.get(params, (err, user) => {
        console.log(user);
        if (err || !user.Item) {
            return errorResponse('No existe un usuario con el email ingresado.', context.awsRequestId, callback);
        } else {
            console.log(user);
            callback(null, {
                statusCode: 200,
                body: JSON.stringify({
                    user: user.Item
                }),
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            });
        }
    });
};

function errorResponse(errorMessage, awsRequestId, callback) {
    callback(null, {
        statusCode: 400,
        body: JSON.stringify({
            errorMessage,
            awsRequestId
        }),
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    });
}