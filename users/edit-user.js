const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

// Edit an user
exports.editUser = (event, context, callback) => {

    const requestBody = JSON.parse(event.body);

    const email = requestBody.email;
    const firstName = requestBody.firstName;
    const lastName = requestBody.lastName;
    const sex = requestBody.sex;
    const birthDate = requestBody.birthDate;
    const nationality = requestBody.nationality;
    const skinTone = requestBody.skinTone;

    // Validate the fields
    if (!email || !firstName || !lastName || !sex || !birthDate || !nationality || !skinTone) {
        console.log('Not valid data');
        console.log(requestBody);
        return errorResponse('Los valores ingresados no son válidos.', context.awsRequestId, callback);
    }

    const searchParams = {
        Key: {
            "email": email
        },
        TableName: process.env.TABLE_NAME
    };

    ddb.get(searchParams, (error, user) => {
        console.log(user);
        if (error) {
            return errorResponse('Se presentó un error realizando la petición.', context.awsRequestId, callback);
        } else if (!user.Item) {
            return errorResponse('No existe un usuario con ese correo en el sistema.', context.awsRequestId, callback);
        } else {
            saveUser(email, firstName, lastName, sex, birthDate, nationality, skinTone, ddb).then(data => {
                callback(null, {
                    statusCode: 200,
                    body: JSON.stringify({
                        message: 'Tus datos fueron actualizados.'
                    }),
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                });
            }).catch(err => {
                console.log(err);
                return errorResponse('Hubo un error en la actualización.', context.awsRequestId, callback);
            });
        }
    });
};

function errorResponse(errorMessage, awsRequestId, callback) {
    callback(null, {
        statusCode: 400,
        body: JSON.stringify({
            errorMessage,
            awsRequestId
        }),
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    });
}

function saveUser(email, firstName, lastName, sex, birthDate, nationality, skinTone, ddb) {
    const params = {
        Item: {
            email,
            firstName,
            lastName,
            sex,
            birthDate,
            nationality,
            skinTone
        },
        TableName: process.env.TABLE_NAME
    }

    return ddb.put(params).promise();
}