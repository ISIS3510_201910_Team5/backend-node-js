import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'
admin.initializeApp()
const rp = require('request-promise');
const cors = require('cors')({origin: true});
const COLLECTION_NAME = 'USER';
const TRACING_COLLECTION = 'TRACING';
const CONSULTATION_COLLECTION = 'CONSULTATION';
const loading = "LOADING";
// const none = "NONE";
const fine = "NO_DANGER";
const pDangerous = "POTENTIALLY_DANGER";
const dangerous = "DANGER";
const mathjs = require('mathjs');


// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const avgSkinCalculator = functions.https.onRequest((request, response) => {
    const AWSurl = 'https://yel62n7bi5.execute-api.us-east-2.amazonaws.com/Prod/leskan/avgskincolor'
    const jsonObject = request.body; //extract body of the request
    const url = jsonObject.url; //extract URL
    console.log(url);
    console.log("the url has been read")
    const user = jsonObject.user; //extract User
    console.log(user);
    console.log("the user has been read")
    const responseJSON = {"url":url} //create the JSON for AWS
    cors(request, response, () => { //call aws function
		if( request.method !== "POST" ) {
			    response.status(401).json({
				message: "Not allowed"
			});
		}
        var options = {
            method: 'POST',
            uri: AWSurl,
            body: responseJSON,
            json: true // Automatically stringifies the body to JSON
        };
        console.log(options);         
        rp(options)
            .then((parsedBody : any) => {
                console.log(parsedBody);
                const awsAvgSkinColor = parsedBody.output
                admin.firestore().doc('USER/'+user).update({skinTone: awsAvgSkinColor})
                .then(res => {
                    console.log('Document updated');
                    console.log('resultado aws: '+awsAvgSkinColor);
                    response.send({"avgSkinColor":awsAvgSkinColor});
                }) //update firestore
                .catch(err =>{
                    console.log(err);// failed update in firestore
                    response.status(502).json({"error":"firestore writing failed"});
                })
            })
            .catch((err:any) => {
                console.log(err);
                console.log("error al enviar request a aws")
                response.status(502).json({"error":"aws lamda not responding"});
            });
	});    
});

export const analizePicture = functions.https.onRequest((request, response) => {
    const message = function(input: number){
        if(input === 1){
            return fine;
        } else if (input === 2){
            return pDangerous;
        } else {
            return dangerous;
        }
    }
    const AWSurl = 'https://yel62n7bi5.execute-api.us-east-2.amazonaws.com/Prod/leskan/preanalysis'
    const jsonObject = request.body; //extract body of the request
    console.log("arriving json:"+jsonObject);
    const urls = jsonObject.urls; //extract URL
//    const isSimulated = false;
    console.log(urls);
    console.log("the url has been read")
    const user = jsonObject.user; //extract User
    console.log(user);
    console.log("the user has been read")
    const followup = jsonObject.tracing;
    const consultation = jsonObject.consultation;
/*    if(isSimulated===true){
        const result = Math.floor(Math.random() * (3 - 1 + 1)) + 1; //1 ok, 2 probably dangerous, 3 dangerous
        const timer = Math.floor(Math.random() * (3000 - 1 + 1)) + 1; 
        admin.firestore().doc('USER/'+user+'/TRACING/'+followup+'/CONSULTATION/'+consultation).update({time: timer})
        .catch(err =>{
            console.log(err);// failed update in firestore
            response.status(502).json({"error":"firestore writing failed at consultation"});
            response.send();
        })
        admin.firestore().doc('USER/'+user+'/TRACING/'+followup+'/CONSULTATION/'+consultation).update({state: message(result)})
        .then(ans => {
            console.log('consultation updated');
            console.log('resultado aws: ' + message(result));
            admin.firestore().doc('USER/'+user+'/TRACING/'+followup).update({state: message(result)})
            .then(dubleans => {
                console.log('followup updated');
                console.log('resultado aws: ' + message(result));
                response.send({
                    "result": result,
                    "message": message(result)
                });
            }) //update firestore
            .catch(err =>{
                console.log(err);// failed update in firestore
                response.status(502).json({"error":"firestore writing failed at tracing"});
                response.send();
            })
        }) //update firestore
        .catch(err =>{
            console.log(err);// failed update in firestore
            response.status(502).json({"error":"firestore writing failed at consultation"});
            response.send();
        })
    } else {*/
        const responseJSON = {"user":user,
                              "followup":followup,
                              "consultation":consultation,
                              "urls":urls} //create the JSON for AWS
        
        cors(request, response, () => { //call aws function
            if( request.method !== "POST" ) {
                response.status(401).json({
                    message: "Not allowed"
                });
                response.send();
            }
            var options = {
                method: 'POST',
                uri: AWSurl,
                body: responseJSON,
                json: true // Automatically stringifies the body to JSON
            };
            console.log(options);         
            rp(options)
                .then((parsedBody : any) => {
                    console.log(parsedBody);
                    const awsAnalysis = parsedBody.output
                    const timer = parsedBody.time
                    const size = ""+parsedBody.size.toFixed(2)
                    const color = ""+parsedBody.color.toFixed(2)
                    admin.firestore().doc('USER/'+user+'/TRACING/'+followup+'/CONSULTATION/'+consultation).update({time: timer})
                    .catch(err =>{
                        console.log(err);// failed update in firestore
                        response.status(502).json({"error":"firestore writing failed at consultation"});
                        response.send();
                    })
                    admin.firestore().doc('USER/'+user+'/TRACING/'+followup).update({colorChange: color})
                    .catch(err =>{
                        console.log(err);// failed update in firestore
                        response.status(502).json({"error":"firestore writing failed at followup"});
                        response.send();
                    })
                    admin.firestore().doc('USER/'+user+'/TRACING/'+followup).update({sizeChange: size})
                    .catch(err =>{
                        console.log(err);// failed update in firestore
                        response.status(502).json({"error":"firestore writing failed at followup"});
                        response.send();
                    })
                    admin.firestore().doc('USER/'+user+'/TRACING/'+followup+'/CONSULTATION/'+consultation).update({state: message(awsAnalysis)})
                    .then(ans => {
                        console.log('consultation updated');
                        console.log('resultado aws: ' + message(awsAnalysis));
                        admin.firestore().doc('USER/'+user+'/TRACING/'+followup).update({state: message(awsAnalysis)})
                        .then(dubleans => {
                            console.log('followup updated');
                            console.log('resultado aws: ' + message(awsAnalysis));
                            response.send({
                                "result": awsAnalysis,
                                "message": message(awsAnalysis)
                            });
                        }) //update firestore
                        .catch(err =>{
                            console.log(err);// failed update in firestore
                            response.status(502).json({"error":"firestore writing failed at tracing"});
                            response.send();
                        })
                    }) //update firestore
                    .catch(err =>{
                        console.log(err);// failed update in firestore
                        response.status(502).json({"error":"firestore writing failed at consultation"});
                        response.send();
                    })
                })
                .catch((err:any) => {
                    console.log(err);
                    response.status(502).json({"error":"aws lamda not responding"});
                    response.send();
                });
        });   
});

export const sendNewNotification =functions.firestore.document('USER/{user}/TRACING/{followup}').onWrite((change, context) => {
    const uuid = context.params.user;

    console.log('User to send notification', uuid);

    // The topic name can be optionally prefixed with "/topics/".
    var topic = 'notification';

    var message = {
        notification: {
            title: 'El resultado de tu análisis se encuentra listo',
            body: 'Haz clic aquí para verlo'
        },
        topic: topic
    };

    // Send a message to devices subscribed to the provided topic.
    admin.messaging().send(message)
        .then((response) => {
            // Response is a message ID string.
            console.log('Successfully sent message:', response);
        })
        .catch((error) => {
            console.log('Error sending message:', error);
        });
});

export const analizeFollowUp = functions.https.onRequest((request, response) => {
    const message = function(input: number){
        if(input === 1){
            return fine;
        } else if (input === 2){
            return pDangerous;
        } else {
            return dangerous;
        }
    }
    const AWSurl = 'https://yel62n7bi5.execute-api.us-east-2.amazonaws.com/Prod/'
    const jsonObject = request.body; //extract body of the request
    const urls = jsonObject.urls; //extract URL
    const isSimulated = jsonObject.isSimulated;
    console.log(urls);
    console.log("the url has been read")
    if(isSimulated===true){
        const result = Math.floor(Math.random() * (3 - 1 + 1)) + 1; //1 ok, 2 probably dangerous, 3 dangerous
        return response.status(200).json({
            "result": result,
            "message": message(result)
        });
    } else {
        const responseJSON = {"urls":urls} //create the JSON for AWS
        cors(request, response, () => { //call aws function
            if( request.method !== "POST" ) {
                return response.status(401).json({
                    message: "Not allowed"
                });
            }
            var options = {
                method: 'POST',
                uri: AWSurl,
                body: responseJSON,
                json: true // Automatically stringifies the body to JSON
            };
            console.log(options);         
            rp(options)
                .then((parsedBody : any) => {
                    console.log(parsedBody);
                    const awsAnalysis = parsedBody.output
                    return response.status(200).json({
                        "result": awsAnalysis,
                        "message": message(awsAnalysis)
                    });
                })
                .catch((err:any) => {
                    console.log(err);
                });
            return response.status(502).json({"error":"aws lamda not responding"});
        });   
    }
    return response.status(502).json({"error":"server error"});
});

export const rapidResponseASC = functions.https.onCall((data, context) =>{
    const requesturl = 'https://us-central1-leskan-app.cloudfunctions.net/avgSkinCalculator';
    const user = data.user;
    const url = data.url;
    const responseJSON = {"user": user,"url": url}; //create body of the request in JSON
    var options = {
        method: 'POST',
        uri: requesturl,
        body: responseJSON,
        json: true // Automatically stringifies the body to JSON
    };
    console.log(options);         
    rp(options)
        .then((parsedBody : any) => {
            console.log(parsedBody);
        })
        .catch((err:any) => {
            console.log(err);
            console.log("error al enviar request")
            return {"error":"error al enviar request"};
        });
    return {"status":"ok"};
});

export const rapidResponseAnalizer = functions.https.onCall((data, context) =>{
    const requesturl = 'https://us-central1-leskan-app.cloudfunctions.net/analizePicture'
    console.log("request body: "+ data)
    const user = data.user;
    const urls = data.urls;
    const tracing = data.tracing;
    const consultation = data.consultation;
    const isSimulated = data.isSimulated;
    const responseJSON = {
        "user": user,
        "urls": urls,
        "tracing": tracing,
        "consultation": consultation,
        "isSimulated": isSimulated
    }; //create body of the request in JSON
    var options = {
        method: 'POST',
        uri: requesturl,
        body: responseJSON,
        json: true // Automatically stringifies the body to JSON
    };
    console.log(options);         
    rp(options)
        .then((parsedBody : any) => {
            console.log(parsedBody);
        })
        .catch((err:any) => {
            console.log(err);
            console.log("error al enviar request")
            return {"error":"error al enviar request"};
        });
    return {"status":"ok"};
});

export const numeroDeConsultas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    async function callConsultations(id:string,name:string){
        try {
            const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
            const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
            if (consultationArray.length === 0){
               console.log("no consults in tracing: "+name+"with in user: "+id);
            }
            for(const consulta of consultationArray){
                const fechaConsulta = tDate(consulta.date)
                if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta){
                    sumOfTime = sumOfTime + 1;
                }
            }
        } catch (error) {
            console.log("no consults in tracing: "+name+"with in user: "+id);
        } 
    }
    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            const tracingName: string[] = [];
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                tracingName.push(tracing.name);
            }
            for (const name of tracingName){
                await callConsultations(id,name);
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        response.send({"numeroDeConsultas":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeUsuarios = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        console.log(data.length);
        response.send({"usuarios":data.length});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const sexoPredominante = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    let masculino = 0;
    let femenino = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        for (let user of data){
            if(user.sex === 1){
                masculino = masculino + 1;
            } else {
                femenino = femenino + 1;
            }
        }
        const respuesta = {"masculino": masculino, "femenino":femenino} 
        console.log(respuesta);
        response.send(respuesta);
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const tonoDePielPredominante = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        let color:number[] = [];
        for (let user of data){
            color.push(user.skinTone) ;
        }
        const standarDeviation = mathjs.std(color);
        const average = mathjs.mean(color);
        response.send({"standarDeviation":standarDeviation,"average":average});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const rangoDeEdad = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    let rangos:number[] = []; 
    for (let i = 1; i < 17; i++){
        rangos.push(i*5);
    }
    let nUsuarios:number[] = [];
    for (let i = 1; i < 17; i++){
        nUsuarios.push(0);
    }
    const calEdad = function(dob:Date) { 
        var diff_ms = Date.now() - dob.getTime();
        var age_dt = new Date(diff_ms);       
        return Math.abs(age_dt.getUTCFullYear() - 1970);
    }
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        for(let user of data){
            let stringTransform = user.birthDate;
            let dateParts:string[] = stringTransform.split("/") 
            let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
            let age = calEdad(date);
            console.log({"fecha": date, "edad": age})
            let counter = 0;
            let terminado = false;
            for(let rango of rangos){
                let lMenor = rango - 4; 
                if(age < rango && age > lMenor && terminado == false){
                    nUsuarios[counter]++;
                    terminado = true;
                } else if (rango === 80 && terminado == false){
                    //nUsuarios[counter]++;
                    terminado = true;
                }
                counter++;
            }
        }
        let count = 0;
        let max = -1;
        let range = 0;
        for(let rango of nUsuarios){
            if(rango>max){
                max = rango;
                range = count;
            }
            count++;
        }
        const limInf = rangos[range]-4;
        const limSup = rangos[range];
        const respuesta = {
            "limInf": limInf,
            "limSup": limSup,
            "numeroUsuarios":max
        }
        console.log(respuesta);
        response.send(respuesta);
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeConsultasPeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.state == dangerous){
                                sumOfTime = sumOfTime + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        response.send({"numeroDeConsultas":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeConsultasNoPeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.state === fine){
                                sumOfTime = sumOfTime + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        response.send({"numeroDeConsultas":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeConsultasPosiblementePeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.state === pDangerous){
                                sumOfTime = sumOfTime + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        response.send({"numeroDeConsultas":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeConsultasSinAnalizar = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.state === loading){
                                sumOfTime = sumOfTime + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        response.send({"numeroDeConsultas":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const promedioDeConsultasPorUsuario = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    let numberOfUsers = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        numberOfUsers = data.length;
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta){
                                sumOfTime = sumOfTime + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        const avg = sumOfTime/numberOfUsers;
        response.send({"promedioDeConsultas":avg});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
}); 

export const tiempoDeAnalisisPromedio = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    let numberOfUsers = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        numberOfUsers = data.length;
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.time!=null){
                                sumOfTime = sumOfTime + consult.time;
                                console.log("consult time: "+consult.time+", user: "+id+", tracing:"+name+", consult:"+consult.id)
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        const avg = sumOfTime/numberOfUsers;
        response.send({"promedioDeTiempoPorConsultas":avg});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
}); 

export const tiempoDeAnalisisPromedioPorGenero = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTimeMen = 0;
    let sumOfTimeWomen = 0;
    let numberOfMen = 0;
    let numberOfWomen = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const users: FirebaseFirestore.DocumentData[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            users.push(user);
        } 
        for(let user of users){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(user.id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+user.id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(user.id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+user.id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&user.sex === 1&&consult.time!=null){
                                sumOfTimeMen = sumOfTimeMen + consult.time;
                                numberOfMen = numberOfMen + 1;
                            } else if (fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&user.sex === 0&&consult.time!=null){
                                sumOfTimeWomen = sumOfTimeWomen + consult.time;
                                numberOfWomen = numberOfWomen + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+user.id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+user.id);
            } 
        }
        const avgMen = sumOfTimeMen/numberOfMen;
        const avgWomen = sumOfTimeWomen/numberOfWomen;
        response.send({"promedioDeTiempoPorConsultasHombres":avgMen,
                        "promedioDeTiempoPorConsultasMujeres":avgWomen});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
}); 

export const tiempoDeAnalisisPromedioConsultasNoPeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    let numberOfUsers = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.time!=null&&consult.state === fine){
                                sumOfTime = sumOfTime + consult.time;
                                numberOfUsers = numberOfUsers + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        const avg = sumOfTime/numberOfUsers;
        response.send({"promedioDeTiempoPorConsultas":avg});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
}); 

export const tiempoDeAnalisisPromedioConsultasPosiblementePeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    let numberOfUsers = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.time!=null&&consult.state === pDangerous){
                                sumOfTime = sumOfTime + consult.time;
                                numberOfUsers = numberOfUsers + 1;
                                console.log("consult time: "+consult.time+", user: "+id+", tracing:"+name+", consult:"+consult.id)
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        console.log("sum of time: "+sumOfTime+", number of consults: "+numberOfUsers)
        const avg = sumOfTime/numberOfUsers;
        response.send({"promedioDeTiempoPorConsultas":avg});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
}); 

export const tiempoDeAnalisisPromedioConsultasPeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        let stringTransform = fecha;
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    const tDate = function(fecha:string){
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split(" - ")
        let dateParts:string[] = firstSplit[0].split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        return date;
    }
    let sumOfTime = 0;
    let numberOfUsers = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                        }
                        console.log("consultation array lenght: "+consultationArray.length);
                        for (let consult of consultationArray){
                            const fechaConsulta = tDate(consult.date)
                            if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&consult.time!=null&&consult.state === dangerous){
                                sumOfTime = sumOfTime + consult.time;
                                numberOfUsers = numberOfUsers + 1;
                            }
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
            } 
        }
        const avg = sumOfTime/numberOfUsers;
        response.send({"promedioDeTiempoPorConsultas":avg});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
}); 

export const numeroDeUsuariosSinConsultas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    let sumOfTime = 0;
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (let user of data){
            usersId.push(user.id);
        } 
        for(let id of usersId){
            try{
                const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
                const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
                const tracingName: string[] = [];
                if (tracingArray.length == 0){
                    console.log("no tracing in user: "+id);
                }
                for (let tracing of tracingArray){
                    tracingName.push(tracing.name);
                }
                for (let name of tracingName){
                    try {
                        const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
                        const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
                        if (consultationArray.length == 0){
                           console.log("no consults in tracing: "+name+"with in user: "+id);
                           sumOfTime = sumOfTime + 1;
                        }
                    } catch (error) {
                        console.log("no consults in tracing: "+name+"with in user: "+id);
                        sumOfTime = sumOfTime + 1;
                    } 
                }
            } catch (error) {
                console.log("no tracing in user: "+id);
                sumOfTime = sumOfTime + 1;
            } 
        }
        response.send({"numeroDeConsultas":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeSiguimientosConMasDeUnaConsulta = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;

    async function callConsultations(id:string,name:string){
        try {
            const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
            const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
            let counted = false;
            if (consultationArray.length > 2){
                for(const consulta of consultationArray){
                    const fechaConsulta = tDate(consulta.date)
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta && counted === false){
                        counted = true;
                        sumOfTime = sumOfTime + 1;
                    }
                }
            }
        } catch (error) {
            console.log("no consults in tracing: "+name+"with in user: "+id);
        } 
    }

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            const tracingName: string[] = [];
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                tracingName.push(tracing.name);
            }
            for (const name of tracingName){
                await callConsultations(id,name);
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }

    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        response.send({"numeroDeSeguimientos":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeSiguimientosPeligrosos = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;

    async function callConsultations(id:string,name:string){
        try {
            const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
            const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
            let counted = false;
            if (consultationArray.length > 2){
                for(const consulta of consultationArray){
                    const fechaConsulta = tDate(consulta.date)
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta && counted === false){
                        counted = true;
                        sumOfTime = sumOfTime + 1;
                    }
                }
            }
        } catch (error) {
            console.log("no consults in tracing: "+name+"with in user: "+id);
        } 
    }

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.state === dangerous){
                    await callConsultations(id,tracing.name);
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        response.send({"numeroDeSeguimientos":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeSiguimientosPosiblementePeligrosos = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;

    async function callConsultations(id:string,name:string){
        try {
            const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
            const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
            let counted = false;
            if (consultationArray.length > 2){
                for(const consulta of consultationArray){
                    const fechaConsulta = tDate(consulta.date)
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta && counted === false){
                        counted = true;
                        sumOfTime = sumOfTime + 1;
                    }
                }
            }
        } catch (error) {
            console.log("no consults in tracing: "+name+"with in user: "+id);
        } 
    }

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.state === pDangerous){
                    await callConsultations(id,tracing.name);
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        response.send({"numeroDeSeguimientos":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const numeroDeSiguimientosNoPeligrosos = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;

    async function callConsultations(id:string,name:string){
        try {
            const consultationSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).doc(name).collection(CONSULTATION_COLLECTION).get()
            const consultationArray = consultationSnapshot.docs.map(doc => doc.data());
            let counted = false;
            if (consultationArray.length > 2){
                for(const consulta of consultationArray){
                    const fechaConsulta = tDate(consulta.date)
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta && counted === false){
                        counted = true;
                        sumOfTime = sumOfTime + 1;
                    }
                }
            }
        } catch (error) {
            console.log("no consults in tracing: "+name+"with in user: "+id);
        } 
    }

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.state === fine){
                    await callConsultations(id,tracing.name);
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        response.send({"numeroDeSeguimientos":sumOfTime});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const cambioDeColorPromedioEnSeguimientos = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.colorChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.colorChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&check>0){
                        const c = parseFloat(tracing.colorChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const cambioDeColorPromedioEnSeguimientosNoPeligrosos = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.colorChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.colorChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&tracing.state===fine&&check>0){
                        const c = parseFloat(tracing.colorChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const cambioDeColorPromedioEnSeguimientosPosiblementePeligrosos = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.colorChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.colorChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&tracing.state===pDangerous&&check>0){
                        const c = parseFloat(tracing.colorChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const cambioDeColorPromedioEnSeguimientosPeligrosos = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.colorChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.colorChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&tracing.state===dangerous&&check>0){
                        const c = parseFloat(tracing.colorChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const averageSizeChange = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.sizeChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.sizeChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&check>0){
                        const c = parseFloat(tracing.sizeChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const averageSizeChangeNoPeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.sizeChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.sizeChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&tracing.state===fine&&check>0){
                        const c = parseFloat(tracing.sizeChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const averageSizeChangePosiblementePeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.sizeChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.sizeChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&tracing.state===pDangerous&&check>0){
                        const c = parseFloat(tracing.sizeChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});

export const averageSizeChangePeligrosas = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', "*")
    response.set('Access-Control-Allow-Methods', 'GET')
    const tFecha = function(fecha:string){
        console.log("fecha por parametro: "+fecha)
        let stringTransform = fecha.trim();
        let dateParts:string[] = stringTransform.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const tDate = function(fecha:string){
        console.log("fecha consulta por parametro: "+fecha)
        let stringTransform = fecha;
        let firstSplit:string[] = stringTransform.split("-")
        const dayMonthYear=firstSplit[0].trim();
        let dateParts:string[] = dayMonthYear.split("/") 
        let date = new Date(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2]);
        console.log("fecha transformada: "+date.toDateString)
        return date;
    }
    const fechaInicio = tFecha(request.query.fechaInicio);
    const fechaFin = tFecha(request.query.fechaFin);
    let sumOfTime = 0;
    let numberOfTracings = 0;

    async function callTracing(id:string){
        try{
            const tracingSnapshot = await admin.firestore().collection(COLLECTION_NAME).doc(id).collection(TRACING_COLLECTION).get()
            const tracingArray = tracingSnapshot.docs.map(doc => doc.data());
            console.log("tracing array lenght: "+tracingArray.length);
            if (tracingArray.length === 0){
                console.log("no tracing in user: "+id);
            }
            for (const tracing of tracingArray){
                if(tracing.sizeChange !== ""){
                    const fechaConsulta = tDate(tracing.date)
                    const check = parseFloat(tracing.sizeChange) || 0; 
                    if(fechaConsulta>=fechaInicio&&fechaFin>=fechaConsulta&&tracing.state===dangerous&&check>0){
                        const c = parseFloat(tracing.sizeChange);
                        sumOfTime = c + sumOfTime;
                        numberOfTracings = 1 + numberOfTracings; 
                    }
                }
            }
        } catch (error) {
            console.log("no tracing in user: "+id);
        } 
    }
    
    try {
        const snapshot = await admin.firestore().collection(COLLECTION_NAME).get()
        const data = snapshot.docs.map(doc => doc.data());
        const usersId: string[] = [];
        if (data.length == 0){
            console.log("no users");
        }
        for (const user of data){
            usersId.push(user.id);
        } 
        for(const id of usersId){
            await callTracing(id)
        }   
        const pc = sumOfTime/numberOfTracings; 
        response.send({"promedioCambio":pc});
    }
    catch (error) {
        console.log(error)
        response.status(500).send(error)
    } 
});