const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

// Get the skin tones list
exports.getSkinTones = (event, context, callback) => {

    const params = {
        TableName: process.env.TABLE_NAME
    };

    ddb.scan(params, (err, skinTones) => {
        if (err) {
            console.log(err);
            return errorResponse('Se presentó un error realizando la petición.', context.awsRequestId, callback);
        } else if (!skinTones.Items || skinTones.Items.length === 0) {
            return errorResponse('No hay tonos de piel disponibles para elegir.', context.awsRequestId, callback);
        } else {
            callback(null, {
                statusCode: 200,
                body: JSON.stringify({
                    skinTones: skinTones.Items
                }),
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            });
        }
    });
};

function errorResponse(errorMessage, awsRequestId, callback) {
    callback(null, {
        statusCode: 400,
        body: JSON.stringify({
            errorMessage,
            awsRequestId
        }),
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    });
}